$(document).ready(function(){ // Fait demarrer le script quand le DOM a fini à charger.
var $jours = $('.jour');
var $puces = $('.bullets .entypo-record');

	function init(){
		setTimeout(function(){
			$('body').addClass('isok'); // ajout de la classe Isok au Body, ajoutant un fond clair.
			$jours.hide(); // cache toutes les informations météo sur les jours.
			$('.wrapper').fadeIn('slow', function(){ // Fait afficher en foudu le contenu de la div .wrapper, en display:none jusqu'ici.
				$jours.first().fadeIn('slow'); // Fait afficher  en fondu le contenu de la première balise ayant pour class "jour". 
				$puces.removeClass('active').first().addClass('active'); //Enlève la classe "active" à toutes les balises ayant la classe "entypo-record", descendantes d'une balise ayant la class "bullets", puis l'ajoute à la première d'entre elles.
			});
		}, 2000); // La fonction est lancé après 2s de delai.	
	}
	$puces.click(function(){ //quand une des balises ayant la classe "entypo-record", descendantes d'une balise ayant la class "bullets", est cliquée, lancer la fonction suivante.
		var $this = $(this);
		var cible = $this.attr('data-cible'); // enregistre dans une variable nommée cible la valeur de l'attribut data-cible de l'element cliqué.
		$jours.hide(); // masque le contenu de toutes les balises ayant la classe "jour".

		$($jours.get(cible)).fadeIn(); // fait apparaitre en fondu, le contenu de la balise ayant la classe "jour" et dont la place dans le DOM correspond à la valeur enregistrée dans la variable "cible".
		$puces.removeClass('active'); // Enlève la classe "active" à toutes les balises ayant la classe "entypo-record", descendantes d'une balise ayant la class "bullets".
		$this.addClass('active'); // ajoute la classe "active" à l'élément cliqué.
	});
	init(); // Lancer la fonction init() à la lecture de cette ligne, quand la fonction parente est appelée. Dans le cas présente, dès que le DOM a fini de charger. 
});